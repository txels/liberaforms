"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2020 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

# use for development only

from liberaforms import app
app.run(host='127.0.0.1', port=5000, debug=True)
